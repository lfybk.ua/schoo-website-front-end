import url from "./url"
import inquiry from "./inquiry";

const auth = {
  async login(data) {
    return await inquiry.post({
      url: url.login,
      sendData: data
    })
  }
}

export default auth;