import url from "./url"
import inquiry from "./inquiry";

const feedback = {
  async setFeedback(data) {
    return await inquiry.post({
      url: url.feedback,
      sendData: data
    }) || null
  }
}

export default feedback;