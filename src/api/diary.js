import url from "./url"
import inquiry from "./inquiry";

const diary = {
  async getDiary(token) {
    return await inquiry.get({
      url: url.diary,
      headers: {
        authorization: token
      }
    }) || null
  }
}

export default diary;