import url from "./url"
import inquiry from "./inquiry";

const tabs = {
  async getTabs() {
    return await inquiry.get({
      url: url.pageTabs
    }) || null
  },
  async getPage(id) {
    return await inquiry.get({
      url: url.pageTabs,
      param: `/${id}`
    }) || null
  }
}

export default tabs;