import url from "./url"
import inquiry from "./inquiry";

const calendar = {
  async getDate() {
    return await inquiry.get({
      url: url.calendar,
    }) || null
  }
}

export default calendar;