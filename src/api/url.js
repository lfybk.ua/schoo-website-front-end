import { settings } from "../settings";
const dev = settings.dev;
const mainURL = dev ? "http://localhost:5050/api" : "https://school123ff.herokuapp.com/api";

const url = {
  tabs: `${mainURL}/gallery/tabs`,
  gallery: `${mainURL}/gallery`,
  login: `${mainURL}/auth/login`,
  diary: `${mainURL}/diary`,
  calendar: `${mainURL}/calendar`,
  pageTabs: `${mainURL}/tabs`,
  feedback: `${mainURL}/feedback/set`,
}

export default url;