import url from "./url"
import inquiry from "./inquiry";

const gallery = {
  async getTabs() {
    return await inquiry.get({
      url: url.tabs
    }) || null
  },
  async getGallery(id) {
    return await inquiry.get({
      url: url.gallery,
      query: `?id=${id}`
    }) || null
  }
}

export default gallery;