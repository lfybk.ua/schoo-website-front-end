import LoginMenu from "../../component/login-menu";

import "./style.scss";

const Login = () => {
  return (
    <section className="login">
      <LoginMenu />
    </section>
  )
}

export default Login;