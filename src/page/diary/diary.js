import { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";

import "./style.scss";

import diary from "../../api/diary";

import { AuthContext, MarkContext } from "../../context"
import { MarkList, AverageScore, StudentsList } from "../../component/diary";


const Diary = () => {
  const { isAuth, deleteAuthData, auth } = useContext(AuthContext)
  const { setMarkData } = useContext(MarkContext)

  const navigate = useNavigate()

  useEffect(() => {
    if (!isAuth()) {
      navigate("/login")
    }

    const fetchData = async () => {
      const data = await diary.getDiary(auth.key);

      if (data.type) {
        deleteAuthData()
        setTimeout(() => navigate("/login"), 1000)
      } else {
        setMarkData(data)
      }
    }

    fetchData()
  }, [])

  return (
    <section className="diary">
      <MarkList />
      <AverageScore />
      <StudentsList />
    </section>
  )
}

export default Diary;