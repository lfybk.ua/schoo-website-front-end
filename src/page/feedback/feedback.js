import "./style.scss";

import FeedbackForm from "../../component/feedback-form";

const Feedback = () => {
  return (
    <section className="feedback">
      <FeedbackForm />
    </section>
  )
}

export default Feedback;