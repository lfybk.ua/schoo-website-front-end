import { useEffect, useState } from "react";
import {
  CalendarView,
  ListEvents
} from "../../component/calendar";

import "./style.scss";
import calendar from "../../api/calendar";


const Calendar = () => {
  const [list, setList] = useState([])

  useEffect(() => {
    const fetchData = async () => setList((await calendar.getDate()).map(e => ({ ...e, date: new Date(e.date) })))

    fetchData()
  }, [])

  return (
    <section className="calendar">
      <CalendarView list={list} />
      <ListEvents list={list} />
    </section>
  )
}

export default Calendar;