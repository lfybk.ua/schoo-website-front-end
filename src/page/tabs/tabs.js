import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import tabs from "../../api/tabs";

import {
  LeftBar,
  Video,
  Image,
  Text,
  Title,
  SubTitle,
  Doc
} from "../../component/tabs";
import { randomID } from "../../util";

import "./style.scss"

const element = {
  video: (item) => <Video item={item} />,
  image: (item, alr, maxWidth) => <Image item={item} alr={alr} maxWidth={maxWidth} />,
  text: (item) => <Text item={item} />,
  title: (item) => <Title item={item} />,
  subTitle: (item) => <SubTitle item={item} />,
  document: (item) => <Doc item={item} />,
}

const Tabs = () => {
  const { id } = useParams();
  const [data, setData] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      setData(await tabs.getPage(id));
    }

    fetchData();
  }, [id])
  console.log(id, data);

  return (
    <section className="tabs">
      <div className="tabs__left-bar">
        <LeftBar data={data} />
      </div>
      <div className="tabs__main">
        {data.map(e => {
          return (
            <a key={randomID() + "lf"} name={e.id + "tab"}>{element[e.type](e.data, e.alr, e.maxWidth)}</a>
          )
        })}
      </div>
    </section>
  )
}

export default Tabs;