import { Route, Routes } from "react-router-dom";

import {
  GalleryMain,
  GalleryTab
} from "../../component/gallery";

const Gallery = () => {
  return (
    <section>
      <Routes>
        <Route path="/" element={<GalleryMain />} />
        <Route path=":id" element={<GalleryTab />} />
      </Routes>
    </section>
  )
}

export default Gallery;