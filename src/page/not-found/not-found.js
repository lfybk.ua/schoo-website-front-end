import "./style.scss";

import CustomLink from "../../component/custom-link";

const NotFound = () => {
  return (
    <section className="not-found">
      <h2 className="not-found__title">Not-Found</h2>
      <p><CustomLink text="Головна" to="/" component="not-found" /></p>
    </section>
  )
}

export default NotFound;