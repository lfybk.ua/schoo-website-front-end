import { createContext, useEffect, useState } from "react"

export const MarkContext = createContext({});

export function MarkContextProvider({ children }) {
  const [data, setData] = useState({ mark: [], class: [] })

  const getMarkDate = (date) => {
    return data.mark.filter((e) => e.date.slice(0, 10) === date);
  }

  const getAverageScore = () => {
    const obj = {}
    for (let item of data?.mark || []) {
      if (obj[item.subjectName] && item.mark) {
        obj[item.subjectName].sum += item.mark
        obj[item.subjectName].counter++;

        continue;
      }

      obj[item.subjectName] = {
        sum: item.mark,
        counter: 1
      }
    }

    const averageScore = [];

    for (let [key, value] of Object.entries(obj)) {
      averageScore.push([key, value.sum / value.counter])
    }

    return averageScore;
  }

  const setMarkData = (data) => {
    setData(data)
  }

  return (
    <MarkContext.Provider
      value={{
        data,
        setMarkData,
        getMarkDate,
        getAverageScore
      }}>
      {children}
    </MarkContext.Provider>
  )
}