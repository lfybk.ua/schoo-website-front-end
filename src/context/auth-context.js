import { createContext, useEffect, useState } from "react"

export const AuthContext = createContext({});

export function AuthContextProvider({ children }) {
  const [auth, setAuth] = useState({})

  useEffect(() => {
    const key = localStorage.getItem("key");
    const fullName = localStorage.getItem("fullName");
    const phone = localStorage.getItem("phone");
    const role = localStorage.getItem("role");

    if (!key || !fullName || !phone || !role) {
      return;
    }

    setAuth({ key, fullName, phone, role });
  }, [])

  const isAuth = () => {
    return auth.key && auth.fullName && auth.phone && auth.role;
  }

  const deleteAuthData = () => {
    localStorage.removeItem("key")
    localStorage.removeItem("fullName");
    localStorage.removeItem("phone");
    localStorage.removeItem("role");
    setAuth({})
  }

  const setAuthData = (data) => {
    setAuth(data);

    for (let [key, item] of Object.entries(data)) {
      localStorage.setItem(key, item);
    }
  }

  return (
    <AuthContext.Provider
      value={{
        auth,
        setAuthData,
        isAuth,
        deleteAuthData
      }}>
      {children}
    </AuthContext.Provider>
  )
}