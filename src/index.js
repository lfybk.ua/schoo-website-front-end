import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './component/app';

import { WidthContextProvider, AuthContextProvider, MarkContextProvider } from "./context"


ReactDOM.render(
  <React.StrictMode>
    <AuthContextProvider>
      <MarkContextProvider>
        <WidthContextProvider>
          <App />
        </WidthContextProvider>
      </MarkContextProvider>
    </AuthContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

