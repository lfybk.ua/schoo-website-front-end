import ListItem from "../list-item";

import "./style.scss";

import { randomID } from "../../../util"

const ListEvents = ({ list }) => {

  const listEvent = list.filter(e => e.date >= new Date())

  return (
    <div className="list">
      <div className="list__box">
        {listEvent.map((e) => <ListItem item={e} key={randomID()} />)}
      </div>
    </div>
  )
}

export default ListEvents;