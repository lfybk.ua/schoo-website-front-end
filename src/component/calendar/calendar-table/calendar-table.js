import CalendarItem from "../calendar-item";

import "./style.scss"

import { randomID } from "../../../util";

function createArray(date) {
  const calendar = [[]];

  const year = date.getFullYear();
  const month = date.getMonth();

  const startDay = new Date(year, month, 1).getDay();
  const lastDay = new Date(year, month + 1, 0).getDate();

  for (let i = 0; i < startDay - 1 || (startDay === 0 && i < 6); i++) {
    calendar[0][i] = "";
  }

  for (let i = 1, j = 0; i < lastDay + 1; i++) {
    if (calendar[j].length === 7) {
      calendar.push([]);
      j++;
    }

    calendar[j].push(i);
  }

  if (calendar.length < 6) {
    const plug = ["", "", "", "", "", "", ""]

    calendar.push(...calendar.length === 5 ? [plug] : [plug, plug])
  }

  return calendar;
}

const convertListDate = (list) => {
  const newList = {}

  for (let item of list) {
    newList[`${item.date.getMonth()}-${item.date.getDate()}`] = item.event;
  }

  return newList;
}

const CalendarTable = ({ date, list }) => {
  const newList = convertListDate(list);

  return (
    <table className="calendar-table">
      <thead className="calendar-table__header">
        <tr>
          <th>Пн</th>
          <th>Вт</th>
          <th>Ср</th>
          <th>Чт</th>
          <th>Пт</th>
          <th>Сб</th>
          <th>Вс</th>
        </tr>
      </thead>
      <tbody className="calendar-table__body">
        {createArray(date).map(row => <tr key={randomID()}>{
          row.map(item => {
            return <CalendarItem key={randomID()} event={newList[`${date.getMonth()}-${item}`] || null} day={item} />
          })}</tr>)}
      </tbody>
    </table>
  )
}

export default CalendarTable;