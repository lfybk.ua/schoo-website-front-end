import "./style.scss";

const CalendarItem = ({ day, event }) => {
  return (
    <td className="calendar-item">
      <div className={`calendar-item__box ${event ? "calendar-item__active" : ""}`}>
        <p
          className="calendar-item__text"
        >
          {day}
        </p>
        {event ?
          <div
            className="calendar-item__dropdown-text"
          >
            <div className="calendar-item__container">
              <p className="calendar-item__event-text">
                {event}
              </p>
            </div>
          </div> : ""}
      </div>
    </td>
  )
}

export default CalendarItem;