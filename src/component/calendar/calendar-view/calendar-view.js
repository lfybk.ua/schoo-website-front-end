import { useState } from "react";
import CalendarTable from "../calendar-table";

import "./style.scss"

const month = ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"]

const CalendarView = ({ list }) => {
  const [date, setDate] = useState(new Date());

  return (
    <div className="calendar-view">
      <div className="calendar-view__top">
        <button
          className="calendar-view__arrow calendar-view__arrow-left"
          onClick={() => setDate(new Date(date.getFullYear(), date.getMonth() - 1))}
        ></button>
        <p className="calendar-view__date">{date.getFullYear()} {month[date.getMonth()]}</p>
        <button
          className="calendar-view__arrow calendar-view__arrow-right"
          onClick={() => setDate(new Date(date.getFullYear(), date.getMonth() + 1))}
        ></button>
      </div>
      <CalendarTable date={date} list={list} />
    </div>
  )
}

export default CalendarView;