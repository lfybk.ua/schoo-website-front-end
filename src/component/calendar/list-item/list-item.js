import "./style.scss";

const getFullNumber = (number) => Math.floor(number / 10) !== 0 ? number : "0" + number;

const ListItem = ({ item: { event, date } }) => {
  const day = date.getDate()
  const month = date.getMonth()

  return (
    <p className="list-item">
      <span className="list-item__date">{getFullNumber(day)}.{getFullNumber(month + 1)}.{date.getFullYear()}</span> - {event}
    </p>
  )
}

export default ListItem;