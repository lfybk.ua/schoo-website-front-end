import React, { useContext, useEffect, useState } from "react";

import "./style.scss"

import Menu from "./menu";
import Burger from "./burger";

import { WidthContext } from "../../context";

const Bar = () => {
  const [buttonBar, setButtonBar] = useState(null)
  const { isMobile } = useContext(WidthContext)

  const isMobileFlag = isMobile();

  const close = () => {
    setButtonBar(null)
  }

  return (
    <menu className="bar">
      <div className="bar__container">
        <div>
          {isMobileFlag ?
            <div
              className="bar__burger"
              onClick={() => setButtonBar(buttonBar === "BURGER" ? null : "BURGER")}
            >
              <div className="bar__burger-item"></div>
              <div className="bar__burger-item"></div>
              <div className="bar__burger-item"></div>
            </div> : ""
          }
          <Burger flag={buttonBar === "BURGER" || !isMobileFlag} close={close} />
        </div>
        <div className="bar__box">
          <button
            onClick={() => setButtonBar(buttonBar === "MENU" ? null : "MENU")}
            className="bar__menu"
          >Меню</button>
          <Menu flag={buttonBar === "MENU"} close={close} />
        </div>
      </div>
    </menu>
  )
}

export default Bar;