import "./style.scss"

import CustomLink from "../../custom-link"

const Burger = ({ flag, close }) => {
  const style = { transform: `rotateY(${flag ? 0 : 90}deg)` }

  return (
    <nav style={style} className="burger">
      <div className="burger__box" onClick={() => close()}><CustomLink component={"burger"} to="/" text="Головна" /></div>
      <div className="burger__box" onClick={() => close()}><CustomLink component={"burger"} to="/calendar" text="Календар" /></div>
      <div className="burger__box" onClick={() => close()}><CustomLink component={"burger"} to="/gallery" text="Галерея" /></div>
      <div className="burger__box" onClick={() => close()}><CustomLink component={"burger"} to="/login" text="Щоденник" /></div>
      <div className="burger__box" onClick={() => close()}><CustomLink component={"burger"} to="/feedback" text="Зворотний зв'язок" /></div>
    </nav>
  )
}



export default Burger;