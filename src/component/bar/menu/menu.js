import { useEffect, useState } from "react";
import "./style.scss";

import tabs from "../../../api/tabs";

import CustomLink from "../../custom-link";
import { randomID } from "../../../util";

const Menu = ({ flag, close }) => {
  const [links, setLinks] = useState([]);
  const style = { transform: `rotateX(${flag ? 0 : 90}deg)` }

  useEffect(() => {
    const fetchData = async () => {
      setLinks(await tabs.getTabs());
    }

    fetchData()
  }, [])

  return (
    <nav style={style} className="menu">
      {
        links.map((e) => {
          return (
            <div
              key={randomID() + "mn"}
              onClick={() => close()}
            >
              <CustomLink component={"menu"} to={`/tabs/${e.id}`} text={e.name} />
            </div>
          )
        })
      }
    </nav>
  )
}

export default Menu;