import React from "react";
import "./style.scss"

import Header from "../header";
import Bar from "../bar";
import Footer from "../footer";

import Feedback from "../../page/feedback";
import Main from "../../page/main";
import Calendar from "../../page/calendar";
import Gallery from "../../page/gallery";
import Login from "../../page/login";
import Diary from "../../page/diary";
import Tabs from "../../page/tabs";
import NotFound from "../../page/not-found";

import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Header />
      <Bar />
      <main className="main">
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/calendar" element={<Calendar />} />
          <Route path="/gallery/*" element={<Gallery />} />
          <Route path="/login" element={<Login />} />
          <Route path="/diary" element={<Diary />} />
          <Route path="/feedback" element={<Feedback />} />
          <Route path="/tabs/:id" element={<Tabs />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </main>
      <Footer />
    </Router>
  )
}

export default App;