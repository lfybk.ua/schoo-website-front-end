import { useRef, useState } from "react";

import { randomID } from "../../util";

import feedback from "../../api/feedback";

import "./style.scss";

const FeedbackForm = () => {
  const [error, setErrors] = useState([])

  const email = useRef(null);
  const phone = useRef(null);
  const fullName = useRef(null);

  const validator = (e) => {
    e.preventDefault()

    const errorList = [];

    const phoneValue = phone.current.value;
    const fullNameValue = fullName.current.value;
    const emailValue = email.current.value;

    if (!phoneValue || !fullNameValue || !emailValue) {
      errorList.push('Необхідно заповнити всі поля')
    }

    if (phoneValue && !/^[0-9]{10}$/.test(phoneValue)) {
      errorList.push('Номер має бути записаний у форматі 073*******')
    }

    if (fullNameValue && (fullNameValue.length >= 40 || fullNameValue.length <= 5)) {
      errorList.push('ФІО має бути не >=5, та не <=40')
    }

    if (emailValue && !/^(([a-z]{5,30})|([a-z]{1,30}\.[a-z]{1,30}))@[a-z]{1,10}\.[a-z]{1,10}$/.test(emailValue)) {
      errorList.push('Email має бути записаний у форматі xxx.xxx@xxx.xxx')
    }

    if (errorList[0]) {
      setErrors(errorList)
      return;
    }

    send({
      phone: phoneValue,
      fullName: fullNameValue,
      email: emailValue,
    })

    setErrors([])
  }

  const send = async (data) => {
    await feedback.setFeedback(data);

    alert("Перевірте свою електронну скриньку.\nМи зв'яжемося з вами найближчим часом");
  }

  return (
    <div className="feedback-form">
      <form className="feedback-form__form">
        <h2 className="feedback-form__title">Форма зворотнього зв'язку</h2>
        <div className="feedback-form__item">
          <label className="feedback-form__text" htmlFor="email">Email</label>
          <input ref={email} className="feedback-form__field" type="email" placeholder="xxx.xxx@xxx.xxx" id="email" />
        </div>
        <div className="feedback-form__item">
          <label className="feedback-form__text" htmlFor="phone">Номер мобільного телефону</label>
          <input ref={phone} className="feedback-form__field" type="phone" placeholder="073*******" id="phone" />
        </div>
        <div className="feedback-form__item">
          <label className="feedback-form__text" htmlFor="FullName">ФІО</label>
          <input ref={fullName} className="feedback-form__field" type="text" placeholder="ФІО" id="FullName" />
        </div>
        <button onClick={validator} className="feedback-form__btn">Надіслати</button>
      </form>
      <ul className="feedback-form__error-list">
        {error.map((e) => {
          return <p key={`${randomID}login`} className="feedback-form__error">{e}</p>
        })}
      </ul>
    </div>
  )
}

export default FeedbackForm;