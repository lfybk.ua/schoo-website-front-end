import { useEffect, useState } from "react";

import GalleryCard from "../gallery-card";

import "./style.scss";

import gallery from "../../../api/gallery";
import { randomID } from "../../../util/index"

const GalleryMain = () => {
  const [tabs, setTabs] = useState([])

  useEffect(() => {
    const fetchData = async () => setTabs(await gallery.getTabs() || [])

    fetchData()
  }, [])


  return (
    <div className="gallery-main">
      <div className="gallery-main__container">
        {tabs.map((data) => {
          return <GalleryCard key={randomID()} data={data} />
        })}
      </div>
    </div>
  )
}

export default GalleryMain;