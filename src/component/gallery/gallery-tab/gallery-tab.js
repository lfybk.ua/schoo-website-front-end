import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import GallerySlider from "../gallery-slider";
import GalleryMenu from "../gallery-menu";

import "./style.scss"

import gallery from "../../../api/gallery";

const GalleryTab = () => {
  const { id } = useParams()
  const [state, setState] = useState({ images: [], index: 0 })

  useEffect(() => {
    const fetchData = async () => setState(
      {
        ...state,
        images: await gallery.getGallery(id)
      })

    fetchData()
  }, [])

  const next = (i) => {
    if (state.index >= state.images.length - 1) return;

    setState({
      ...state,
      index: i ?? state.index + 1
    })
  }

  const previous = (i) => {
    if (state.index <= 0) return;

    setState({
      ...state,
      index: i ?? state.index - 1
    })
  }

  const goToImage = (i) => {
    if (state.index < i) {
      next(i)
    } else if (state.index > i) {
      previous(i)
    }
  }

  return (
    <div className="gallery-tab">
      <GallerySlider images={state.images} index={state.index} previous={previous} next={next} />
      <h2 className="gallery-tab__title">{state.images?.[state?.index]?.title || ""} <span className="gallery-tab__number">{state.index + 1}/{state.images?.length}</span></h2>
      <GalleryMenu images={state.images} index={state.index} goToImage={goToImage} />
    </div>
  )
}

export default GalleryTab;