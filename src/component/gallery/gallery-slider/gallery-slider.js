import "./style.scss";

const GallerySlider = ({ images, index, next, previous }) => {

  return (
    <div className="gallery-slider">
      <div onClick={() => previous()} className="gallery-slider__arrow gallery-slider__arrow-left">
        <div
          className={`gallery-slider__arrow-box ${index <= 0 ? "gallery-slider__arrow-disabled" : ""}`}
        ></div>
      </div>
      <div className="gallery-slider__images">
        {images.map((e, i) => {
          const url = e.img.indexOf('http') !== -1 ? e.img : `https://drive.google.com/uc?export=view&id=${e.img}`;

          const style = { backgroundImage: `url(${url})` }

          if (index < i) {
            style.left = "100%"
          } else if (index > i) {
            style.left = "-100%"
          } else {
            style.left = "0%"
          }

          return <div className="gallery-slider__img" style={style}></div>
        })}
      </div>
      <div onClick={() => next()} className="gallery-slider__arrow gallery-slider__arrow-right">
        <div
          className={`gallery-slider__arrow-box ${index >= images.length - 1 ? "gallery-slider__arrow-disabled" : ""}`}
        ></div>
      </div>
    </div>
  )
}

export default GallerySlider;