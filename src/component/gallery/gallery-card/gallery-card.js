import "./style.scss";
import { Link } from "react-router-dom";

const GalleryCard = ({ data }) => {
  const url = data.img.indexOf('http') !== -1 ? data.img : `https://drive.google.com/uc?export=view&id=${data.img}`;

  const style = { backgroundImage: `url(${url})` }

  return (
    <div className="gallery-cards">
      <Link to={`${data.id}`} className="gallery-cards__link">
        <div className="gallery-cards__image" style={style}></div>
        <p className="gallery-cards__title">{data.title}</p>
      </Link>
    </div>
  )
}

export default GalleryCard;