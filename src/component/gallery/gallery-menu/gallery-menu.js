import "./style.scss";

import { randomID } from "../../../util";

const GalleryMenu = ({ images, index, goToImage }) => {
  return (
    <div className="gallery-menu">
      {images.map((e, i) => {
        const url = e.img.indexOf('http') !== -1 ? e.img : `https://drive.google.com/uc?export=view&id=${e.img}`;

        const style = { backgroundImage: `url(${url})` }

        return (
          <div onClick={() => goToImage(i)} key={randomID()} className="gallery-menu__card">
            <div className={`gallery-menu__img ${index === i ? "gallery-menu__active" : ""}`} style={style}></div>
            <p className="gallery-menu__title">{e.title}</p>
          </div>
        )
      })}
    </div>
  )
}

export default GalleryMenu;