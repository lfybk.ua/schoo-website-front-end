import { useState, useRef, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";

import "./style.scss";

import { AuthContext } from "../../context"
import { randomID } from "../../util"
import auth from "../../api/auth";

const LoginMenu = () => {
  const [data, setData] = useState({})
  const [error, setError] = useState([])
  const [admin, setAdmin] = useState(null)
  const { setAuthData, auth: authData, isAuth } = useContext(AuthContext)
  const navigate = useNavigate()

  const password = useRef(null);
  const email = useRef(null);

  useEffect(() => {
    if (isAuth()) {
      navigate("/diary")
    }
  }, [])

  useEffect(() => {
    if (data.type === "password") {
      setError(["Невірний пароль"])
    } else if (data.type === "email") {
      setError(["Невірний email"])
    } else if (data.key) {
      setAuthData(data)
      setError([])

      navigate("/diary")
    }
  }, [data])

  const handler = async (e) => {
    e.preventDefault()

    const passwordValue = password.current.value.trim()
    const emailValue = email.current.value.trim()

    if (!emailValue || !passwordValue) return;

    const data = await auth.login({ email: emailValue, password: passwordValue })

    if (data.role === "teacher" || data.role === "admin") {
      setAdmin(data.key)
    } else {
      setData(data)
    }
  }

  return (
    <div className="login-menu">
      <form className="login-menu__form">
        <h2 className="login-menu__title">Щоб переглядати оцінки потрібно увійти в акаунт учня</h2>
        <div className="login-menu__item">
          <label className="login-menu__text" htmlFor="email">Email</label>
          <input ref={email} className="login-menu__field" type="email" placeholder="email" id="email" />
        </div>
        <div className="login-menu__item">
          <label className="login-menu__text" htmlFor="password">Пароль</label>
          <input ref={password} className="login-menu__field" type="password" placeholder="Пароль" id="password" />
        </div>
        <button onClick={handler} className="login-menu__btn">Увійти</button>
        {admin ? <input className="login-menu__text" type="text" value={admin} /> : ""}
      </form>
      <ul className="login-menu__error-list">
        {error.map((e) => {
          return <p key={`${randomID}login`} className="login-menu__error">{e}</p>
        })}
      </ul>
    </div>
  )
}


export default LoginMenu;



