import "./style.scss"

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__container">
        <div className="footer__box">
          <p className="footer__item">Tel: +380734175094</p>
          <p className="footer__item">email: lfubk.ua@gmail.com</p>
          <p className="footer__item">telegram: @volleydude</p>
        </div>
        <div className="footer__box">
          <p className="footer__text">
            Дозволяється та вітається будь-яке неспотворене цитування матеріалів цього ресурсу.
            © 2022 Комунальний заклад освіти "НВК № 4 "СЗШ-ДНЗ (ДС)" ДМР. Всі права захищені.
            Site is designed based on "Klasna Ocinka"
          </p>
        </div>
      </div>
    </footer>
  )
}

export default Footer;