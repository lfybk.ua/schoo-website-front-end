import { randomID } from "../../../util/id";
import "./style.scss";


const Text = ({ item }) => {
  const textItems = item.split('\n')

  return (
    <p className="text" >
      {textItems.map(e => {
        return (<p className="text__item" key={randomID() + "tx"}>{e}</p>)
      })}
    </p>
  )
}

export default Text