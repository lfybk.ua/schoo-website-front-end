import "./style.scss";

const Doc = ({ item }) => {
  return (
    <div className="doc">
      <div className="doc__box">
        <a className="doc__link" href={item}>Відкрити орігінал</a>
        <iframe className="doc__item" src={item}></iframe>
      </div>
    </div>
  )
}

export default Doc;