import { randomID } from "../../../util/id";
import "./style.scss";

const LeftBar = ({ data }) => {

  return (
    <div className="left-bar">
      <ul className="left-bar__list">
        {data.map(({ type, id, data }) => {
          let className = null;
          let text = null;
          switch (type) {
            case "title": {
              className = "left-bar__title"
              text = "◦ " + (data.alt || data)
              break;
            }
            case "subTitle": {
              className = "left-bar__sub-title"
              text = "- " + (data.alt || data)
              break;
            }
            case "text": {
              className = "left-bar__text"
              text = data.alt || data.slice(0, 15) + "..."
              break;
            }
            case "image": {
              className = "left-bar__image"
              text = data.alt || "зображення"
              break;
            }
            case "document": {
              className = "left-bar__doc"
              text = data.alt || "документ"
              break;
            }
            case "video": {
              className = "left-bar__video"
              text = data.alt || "відео"
              break;
            }
          }

          return (
            <li key={randomID() + "lf"} className={`left-bar__item ${className}`}>
              <a href={"#" + id + "tab"}>{text}</a>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default LeftBar;