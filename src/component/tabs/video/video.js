import "./style.scss";


const Video = ({ item }) => {
  const url = item.indexOf('http') != -1 ? item : `http://www.youtube.com/embed/${item}`;

  return (
    <div className="video">
      <iframe
        className="video__item"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        src={url}></iframe>
    </div>
  )
}

export default Video