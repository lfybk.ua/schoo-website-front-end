import "./style.scss";


const Image = ({ item, alt, maxWidth }) => {
  const url = item.indexOf('http') != -1 ? item : `https://drive.google.com/uc?export=view&id=${item}`;

  return (
    <div className="image">
      <img className="image__item" src={url} alt={alt || ""} style={{ maxWidth: maxWidth || 550 }} />
    </div>
  )
}

export default Image