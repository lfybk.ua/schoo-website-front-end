import { useEffect, useState } from "react"

import "./style.scss";

import { sliderItem, randomID } from "../../../util"

const Banner = () => {
  const [slide, setSlide] = useState(0);

  useEffect(() => setTimeout(() => {
    if (slide < sliderItem.length - 1) {
      setSlide(slide + 1)
    } else {
      setSlide(0)
    }
  }, 10000), [slide])

  return (
    <section className="banner">
      <div className="banner__slider">
        {sliderItem.map((e, i) => {
          return <div
            className="banner__slider-item"
            style={{
              "backgroundImage": `url('${e}')`,
              "right": `${slide * 100}%`,
            }}
          ></div>
        })}
      </div>
      <div className="banner__box">
        <h2 className="banner__title">Навчально-виховний комплекс №4</h2>
        <p className="banner__text">Комунальний заклад освіти "середня загальноосвітня школа - дошкільний навчальний заклад"</p>
      </div>
    </section>
  )
}

export default Banner;