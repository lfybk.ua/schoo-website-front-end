import { useContext } from "react";
import { MarkContext } from "../../../context";
import { randomID } from "../../../util";

import "./style.scss"

const StudentsList = () => {
  const { data } = useContext(MarkContext);
  return (
    <div className="student-list">
      <p className="student-list__title">Клас:</p>
      <ol className="student-list__list">
        {
          data.class.map((e) => {
            return <li key={randomID() + "sl"} >{e.fullName}</li>
          })
        }
      </ol>
    </div>
  )
}


export default StudentsList;