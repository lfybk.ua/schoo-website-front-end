export { default as MarkList } from "./mark-list";
export { default as AverageScore } from "./average-score";
export { default as StudentsList } from "./students-list";
