import { useContext, useEffect, useRef, useState } from "react";
import { MarkContext } from "../../../context";

import { randomID } from "../../../util";

import "./style.scss";

const MarkList = () => {
  const { getMarkDate, data } = useContext(MarkContext)
  const [mark, setMark] = useState([])
  const reset = useRef(null);

  useEffect(() => {
    setMark(data?.mark?.slice?.(0, 20) || [])
  }, [data])

  return (
    <div className="mark-list">
      <div className="mark-list__container">
        <p className="mark-list__title">Оцінки:</p>
        <div>
          <input ref={reset} className="mark-list__input" onChange={(e) => {
            setMark(getMarkDate(e.target.value))
          }} type="date" />
          <button className="mark-list__btn" onClick={() => {
            setMark(data?.mark?.slice?.(0, 20) || [])
            reset.current.value = ""
          }}>Останні</button>
        </div>
        <div className="mark-list__box">
          <ul className="mark-list__box-list">
            {
              mark.length ? mark.map((e) => {
                return (
                  <li key={randomID() + "mrl"} className="mark-list__item">
                    <p>
                      <span className="mark-list__date">{e.date.slice(0, 10)}</span>
                      <span className="mark-list__subject">{e.subjectName}</span> -
                      <span className="mark-list__mark">{e.mark ? e.mark : null}</span>
                      {e.text}
                    </p>
                  </li>
                )
              }) : "Оціноки не знайдено"
            }
          </ul>
        </div>
      </div>
    </div>
  )
}

export default MarkList;