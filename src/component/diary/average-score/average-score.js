import { useContext } from "react";
import { MarkContext } from "../../../context";
import { randomID } from "../../../util";

import "./style.scss";

const AverageScore = () => {
  const { getAverageScore, data } = useContext(MarkContext);

  return (
    <div className="average-score">
      <p className="average-score__title">Середній бал:</p>
      <ul className="average-score__list">
        {
          getAverageScore().map((e) => {
            return <li key={randomID() + "as"} className="average-score__item">{e[0]} - {e[1]}</li>
          })
        }
      </ul>
    </div>
  )
}

export default AverageScore;