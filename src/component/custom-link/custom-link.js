import { Link, useMatch, useResolvedPath } from "react-router-dom";

const CustomLink = ({ text, to, component }) => {
  const resolved = useResolvedPath(to)
  const match = useMatch({ path: resolved.pathname, end: true })

  return (
    < Link
      to={to}
    >
      <button className={`${component}__btn ${match ? `${component}__btn-active` : ""}`}>{text}</button>
    </Link >
  )
}

export default CustomLink;